var mycanvas;
var context;
var canvas_border;
var mouse_x;
var mouse_y;

let brush_size = 15;
var brushsize;
var isDrawing = false;
var isCircling = false;
var isRectangling = false;
var isTriangling = false;
var isLining = false;
var isStaring = false;

var color_now = 'black';
var ToolNow;
var image;
var pen;
var eraser;
var highlighter;
var rainbow;
var reset;
var refresh;
var text;
var line;
var star;
var heart;
var circle;
var rectangle;
var triangle;
var undo;
var undoo;
var redo;
var redoo;
var upload;
var download;

var choosecolor;
var choosefont = "Impact";
var fontsize = 12;

let hue = 0;


window.onload = function() { //等到onload前的東西已經load好才會做以下的事情(loke this:window已經被載好 就會執行下列的事情)
    init();
    startListener();
    push();//一開始就要拍一張空白的照片不然會都留下第一筆
    brushsize.oninput = function() {
        brush_size = this.value;
    }
};

/* INIT FUNCTION */

function init() {
    mycanvas = document.getElementById('canvas');
    if(mycanvas.getContext) context = mycanvas.getContext('2d');
    else console.log("fail to get context!");

    canvas_border = mycanvas.getBoundingClientRect();

    /*get到html的那個東西*/
    image = document.getElementsByClassName("btn");

    pen = document.getElementById("pen");
    eraser = document.getElementById("eraser");
    highlighter = document.getElementById("highlighter");
    rainbow = document.getElementById("rainbow");
    reset = document.getElementById("reset");
    text = document.getElementById("text");
    line = document.getElementById("line");
    star = document.getElementById("star");
    heart = document.getElementById("heart");
    circle = document.getElementById("circle");
    rectangle = document.getElementById("rectangle");
    triangle = document.getElementById("triangle");
    undo = document.getElementById("undo");
    redo = document.getElementById("redo");
    upload = document.getElementById("upload");
    download = document.getElementById("download");
    brushsize = document.getElementById("brush-slider-bar");
    choosecolor = document.getElementById("choosecolor");

   
}

/* FUNCTION */

function draw(start_x, start_y, end_x, end_y, tool) {
    context.beginPath();
    context.lineWidth = brush_size; // 5
    context.strokeStyle = color_now;
    context.globalAlpha = 1.0;
    if(tool==="pen") context.globalCompositeOperation = "source-over"; //新加的東西會覆蓋過去
    else if(tool==="eraser") context.globalCompositeOperation = "destination-out";//新加的東西會把它擦掉(橡皮擦)
    else if(tool==="rainbow") {
        context.globalCompositeOperation = "source-over";
        context.strokeStyle = `hsl(${ hue }, 100%, 50%)`;  
        if(hue >= 360) hue = 0;
        hue++;
    }
    else if(tool==="highlighter") {
        context.globalCompositeOperation = "source-over";  //context.globalAlpha=0.3;
        context.globalAlpha = 0.1;
    }
    // draw circle
    context.lineCap = 'round';
    context.lineJoin = 'round';
    // ==================
    
    context.moveTo(start_x, start_y);
    context.lineTo(end_x, end_y);
    context.stroke();
    context.closePath();
}

function choose_color(color) {
    color_now = choosecolor.value;
    context.strokeStyle = color_now; //畫布上的顏色
}
function refresh(){
    context.clearRect( 0, 0, mycanvas.width, mycanvas.height );
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*UNDO&REDO上一步下一步*/
let step = -1;
let canvas_history_array = [];

function push() { //每次有新的動作就拍一張照放進
    step++;
    if (step < canvas_history_array.length) {
        canvas_history_array.length = step;
    }
    canvas_history_array.push(mycanvas.toDataURL()); //當前影像存成 Base64 編碼的字串並放入陣列
}

function undoo() {
    if (step > 0) {
        step--;        
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = canvas_history_array[step]; //載入剛剛存放的影像
        canvaspic.onload = function() {
            context.clearRect(0, 0, mycanvas.width, mycanvas.height);
            context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        }
    } 
}

function redoo() {
    if (step < canvas_history_array.length - 1) { //因為要++
        step++;        
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = canvas_history_array[step]; //載入剛剛存放的影像
        canvaspic.onload = function() {
            context.clearRect(0, 0, mycanvas.width, mycanvas.height);
            context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        }
    } 
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*下載*/
function Download() {
    var dl = document.createElement("a"); //"動態"產生一個a的tag 產後要放進去html內
    document.body.appendChild(dl); //append進body
    dl.href = mycanvas.toDataURL(); //把畫布變成連結
    dl.download = "image.png"; //下載下來的檔案名稱
    dl.click();  //用code按下去 下載
    document.body.removeChild(dl); //在把動態的那個連結刪除 (下載完即刪除就不見)
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*上傳*/
function Upload(e) {
    //e.target.files[0]>>圖片 img存的是這張圖片的URL
    var img = URL.createObjectURL(e.target.files[0]);
    var canvasPic = new Image();
    canvasPic.src = img;
    canvasPic.onload = function () {
        refresh();
        context.drawImage(canvasPic, 0, 0); //把那張圖片畫上去 從左上角開始畫 放進去
        push(); //在拍照
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*文字 字型*/
function addTextbox(x, y) {
    const old_input = document.getElementById('text-box');
    console.log('addTextbox', old_input);
    //Remove點擊旁邊(空白處)就回到畫布上 讓框框消失
    if (old_input) {
        old_input.remove();
        push();
        return;
    }
    var newbox = document.createElement("input");
    // newbox.contentEditable = true;
    newbox.setAttribute("type", "text"); //框框內要輸入文字
    newbox.setAttribute("id", "text-box");
    newbox.style.left = x + 'px'; //框框被畫出的位置
    newbox.style.top = y + 'px';
    //newbox.style.="Type here~❣"；
    newbox.style.fontFamily = choosefont;
    newbox.style.fontSize = fontsize + "px";//筆刷變大框框就會變大
    document.body.appendChild(newbox);

    // blur: an element lost focus
    //當我們移開框框我們就回去抓我們的文字
    newbox.addEventListener("blur", e => {
        context.font = `${fontsize}px ${
            choosefont}`; //字型大小及字體
        context.fillText(newbox.value, x, y); //最後再把它弄去BOX上還有座標設上去 就可以印出文字
    })
}
function changeSize(s){
    //console.log("fontsize :", s);
    fontsize = s;
}
function changefont(f){
    console.log("choosefont :", f);
    choosefont = f;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*畫圓形*/
function draw_circle(circle_x, circle_y, radius){ //圓心、圓心、半徑
    context.lineWidth = brush_size; //粗細
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = canvas_history_array[step]; //載入剛剛存放的影像
    canvaspic.onload = function() { //剛剛畫的東西已經被載好
        refresh();
        context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        context.beginPath();
        context.arc(circle_x, circle_y, radius, 0, 2 * Math.PI);
        context.stroke();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*正長方形*/
function draw_rectangle(rectangle_x, rectangle_y, rectangle_w, rectangle_h){ //起始點座標、寬、高
    context.lineWidth = brush_size;
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = canvas_history_array[step];
    canvaspic.onload = function() {
        refresh();
        context.lineCap = 'round';
        context.lineJoin = 'round';
        context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        context.beginPath();
        context.rect(rectangle_x, rectangle_y, rectangle_w, rectangle_h);
        context.stroke();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*三角形*/
function draw_triangle(triangle_x1, triangle_y1, triangle_x2, triangle_y2, triangle_x3, triangle_y3){ //三個頂點
    context.lineWidth = brush_size;
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = canvas_history_array[step];
    canvaspic.onload = function() {
        refresh();
        context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        context.beginPath();
        context.lineCap = 'round';
        context.lineJoin = 'round';
        context.moveTo(triangle_x1, triangle_y1);
        context.lineTo(triangle_x2, triangle_y2);
        context.lineTo(triangle_x3, triangle_y3);
        context.lineTo(triangle_x1, triangle_y1);
        context.stroke();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*直線*/
function draw_line(line_x1, line_y1, line_x2, line_y2) {
    context.lineWidth = brush_size;
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = canvas_history_array[step]; //載入剛剛存放的影像
    canvaspic.onload = function() {
        refresh();
        context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        context.beginPath();
        context.lineCap = 'round';
        context.lineJoin = 'round';
        context.moveTo(line_x1, line_y1);
        context.lineTo(line_x2, line_y2);
        context.stroke();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*星星*/
function draw_star(context,n, r1){//n角星  r1大圓半徑
    context.lineWidth = brush_size;
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = canvas_history_array[step];
    canvaspic.onload = function() {
        refresh();
        context.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        context.beginPath();
        context.lineCap = 'round';
        context.lineJoin = 'round';
        let cor = 360 / n / 2; //獲取固定角度的座標 
        let rightAngle = 90; //y軸座標為起點畫圖
        let r2 = r1 / 2;
        context.translate(r1, r1); //將座標原點弄到圓心
        context.moveTo(0,r1);//將畫筆起始點一道y軸上的頂點
        context.beginPath();
        for (var i = 0; i <= n; i++) {
            //畫第一條邊
            let x1 = r1 * Math.sin((rightAngle + 2 * i * cor) / 180 * Math.PI);
            let y1 =  r1 * Math.cos((rightAngle + 2 * i * cor) / 180 * Math.PI);
            //畫第二條邊
            let x2 = r2 * Math.sin((rightAngle + (2 * i + 1) * cor) / 180 * Math.PI);
            let y2 = r2 * Math.cos((rightAngle + (2 * i + 1) * cor) / 180 * Math.PI);
            
            context.lineTo(x1, y1);
            context.lineTo(x2, y2);
        }
        context.stroke();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*彩虹筆*/
function draw_rainbow(){
    let hue = 0;
    context.strokeStyle = `hsl(${ hue }, 100%, 50%)`;  
    if(hue >= 360) hue = 0;
    hue++;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*icon 換鼠標*/
function change_icon(){
    if(ToolNow === "pen"){ mycanvas.style.cursor = "url('./icon/2.png'), auto"; }
    if(ToolNow === "eraser"){ mycanvas.style.cursor = "url('./icon/1.png'), auto"; }
    if(ToolNow === "rainbow"){ mycanvas.style.cursor = "url('./icon/3.png'), auto"; }
    if(ToolNow === "highlighter"){ mycanvas.style.cursor = "url('./icon/4.png'), auto"; }
    if(ToolNow === "line"){ mycanvas.style.cursor = "url('./icon/6.png'), auto"; }
    if(ToolNow === "star"){ mycanvas.style.cursor = "url('./icon/10.png'), auto"; }
    if(ToolNow === "heart"){ mycanvas.style.cursor = "url('./icon/9.png'), auto"; }
    if(ToolNow === "circle"){ mycanvas.style.cursor = "url('./icon/7.png'), auto"; }
    if(ToolNow === "rectangle"){ mycanvas.style.cursor = "url('./icon/5.png'), auto"; }
    if(ToolNow === "triangle"){ mycanvas.style.cursor = "url('./icon/8.png'), auto"; }
    if(ToolNow === "text"){ mycanvas.style.cursor = "url('./icon/11.png'), auto"; }
}


/* Event listener */
function startListener() { /*裡面放要觸發的事件*/
    mycanvas.addEventListener('click', e => {
        if(ToolNow === 'text') {
            addTextbox(e.clientX, e.clientY);
        }
    });
    /*滑鼠*/
    mycanvas.addEventListener('mousedown', e => {
        mouse_x = e.clientX - canvas_border.left;
        mouse_y = e.clientY - canvas_border.top;
        if (ToolNow === 'pen' || ToolNow === 'eraser' || ToolNow === 'rainbow'|| ToolNow === 'highlighter') {
            isDrawing = true;
            draw(mouse_x, mouse_y, mouse_x, mouse_y, ToolNow);
        }
        else if (ToolNow === 'circle') {
            // console.log("mouse down: in draw circle");
            isCircling = true;
            context.globalCompositeOperation = "source-over";
            //一開始點下去的點 固定不動(start_x&start_y)
            start_x = e.clientX - canvas_border.left;
            start_y = e.clientY - canvas_border.top;
        }
        else if (ToolNow === 'rectangle') {
            isRectangling = true;
            context.globalCompositeOperation = "source-over";
            start_x = e.clientX - canvas_border.left;
            start_y = e.clientY - canvas_border.top;
        }
        else if (ToolNow === "triangle") {
            isTriangling = true;
            context.globalCompositeOperation = "source-over";
            //一開始點下去的點 固定不動(start_x&start_y)(最上面那點)
            start_x = e.clientX - canvas_border.left;
            start_y = e.clientY - canvas_border.top;
        }
        else if (ToolNow === "line") {
            isLining = true;
            context.globalCompositeOperation = "source-over";
            start_x = e.clientX - canvas_border.left;
            start_y = e.clientY - canvas_border.top;
        }
        else if (ToolNow === "star"){
            isStaring = ture;
            context.globalCompositeOperation = "source-over";
            draw_star();
        }
    });

    mycanvas.addEventListener('mousemove', e => {
        if (isDrawing) {
            draw(mouse_x, mouse_y, e.clientX - canvas_border.left, e.clientY - canvas_border.top, ToolNow);
            mouse_x = e.clientX - canvas_border.left;
            mouse_y = e.clientY - canvas_border.top;
        }
        else if (isCircling) {
            // console.log("mouse move: in draw circle");
            //拖曳一直在變化的點(mouse_x&mouse_y)
            mouse_x = e.clientX - canvas_border.left;
            mouse_y = e.clientY - canvas_border.top;
            //變數 算圓心跟直徑(diff_x&diff_y)
            const diff_x = mouse_x - start_x; //變化的點-一開始的點
            const diff_y = mouse_y - start_y;
            //兩點之間距離公式 :|AB|=開根號(x1-x2)^2+(y1-y2)^2
            //圓心、圓心、半徑
            draw_circle((start_x + mouse_x) / 2, (start_y + mouse_y) / 2, Math.sqrt(diff_x * diff_x + diff_y * diff_y) / 2);
        }
        else if (isRectangling) {
            mouse_x = e.clientX - canvas_border.left;
            mouse_y = e.clientY - canvas_border.top;
            const diff_x = mouse_x - start_x;
            const diff_y = mouse_y - start_y;
            draw_rectangle(start_x, start_y, diff_x, diff_y);
        }
        else if (isTriangling) {
            mouse_x = e.clientX - canvas_border.left;
            mouse_y = e.clientY - canvas_border.top;
            const diff_x = mouse_x - start_x;
            const diff_y = mouse_y - start_y;
            draw_triangle(start_x, start_y, start_x - diff_x, mouse_y, mouse_x, mouse_y);
        }
        else if (isLining) {
            mouse_x = e.clientX - canvas_border.left;
            mouse_y = e.clientY - canvas_border.top;
            draw_line(start_x, start_y, mouse_x, mouse_y);
        }
    });

    mycanvas.addEventListener('mouseup', e => {
        if (isDrawing) {
            context.globalCompositeOperation = "source-over"; // 0405, change back to source-over after using the eraser
            isDrawing = false;
            push();
        }
        else if (isCircling) {
            isCircling = false;
            push(); //每次有新的動作就拍一張照放進
        }
        else if (isRectangling) {
            isRectangling = false;
            push();
        }
        else if (isTriangling) {
            isTriangling = false;
            push();
        }
        else if (isTriangling) {
            isTriangling = false;
            push();
        }
        else if (isLining) {
            isLining = false;
            push();
        }
        else if (isStaring){
            isStaring = false;
            push();
        }
    });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*游標滑到>>圖標變色*/
    image[0].addEventListener('mouseover', e => { 
        if(!(ToolNow == "pen"))image[0].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[0].addEventListener('mouseout', e => { 
        if(!(ToolNow == "pen"))image[0].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[1].addEventListener('mouseover', e => { 
        if(!(ToolNow == "eraser"))image[1].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[1].addEventListener('mouseout', e => { 
        if(!(ToolNow == "eraser"))image[1].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[2].addEventListener('mouseover', e => { 
        if(!(ToolNow == "highlighter"))image[2].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[2].addEventListener('mouseout', e => { 
        if(!(ToolNow == "highlighter"))image[2].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[3].addEventListener('mouseover', e => { 
        if(!(ToolNow == "rainbow"))image[3].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[3].addEventListener('mouseout', e => { 
        if(!(ToolNow == "rainbow"))image[3].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[4].addEventListener('mouseover', e => { 
        if(!(ToolNow == "reset"))image[4].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[4].addEventListener('mouseout', e => { 
        if(!(ToolNow == "reset"))image[4].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });    
    image[5].addEventListener('mouseover', e => { 
        if(!(ToolNow == "text"))image[5].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[5].addEventListener('mouseout', e => { 
        if(!(ToolNow == "text"))image[5].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[6].addEventListener('mouseover', e => { 
        if(!(ToolNow == "line"))image[6].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[6].addEventListener('mouseout', e => { 
        if(!(ToolNow == "line"))image[6].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[7].addEventListener('mouseover', e => { 
        if(!(ToolNow == "star"))image[7].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[7].addEventListener('mouseout', e => { 
        if(!(ToolNow == "star"))image[7].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[8].addEventListener('mouseover', e => { 
        if(!(ToolNow == "heart"))image[8].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[8].addEventListener('mouseout', e => { 
        if(!(ToolNow == "heart"))image[8].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[9].addEventListener('mouseover', e => { 
        if(!(ToolNow == "circle"))image[9].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[9].addEventListener('mouseout', e => { 
        if(!(ToolNow == "circle"))image[9].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[10].addEventListener('mouseover', e => { 
        if(!(ToolNow == "rectangle"))image[10].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[10].addEventListener('mouseout', e => { 
        if(!(ToolNow == "rectangle"))image[10].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[11].addEventListener('mouseover', e => { 
        if(!(ToolNow == "triangle"))image[11].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[11].addEventListener('mouseout', e => { 
        if(!(ToolNow == "triangle"))image[11].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[12].addEventListener('mouseover', e => { 
        if(!(ToolNow == "undo"))image[12].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[12].addEventListener('mouseout', e => { 
        if(!(ToolNow == "undo"))image[12].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[13].addEventListener('mouseover', e => { 
        if(!(ToolNow == "redo"))image[13].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[13].addEventListener('mouseout', e => { 
        if(!(ToolNow == "redo"))image[13].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[14].addEventListener('mouseover', e => { 
        if(!(ToolNow == "upload"))image[14].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[14].addEventListener('mouseout', e => { 
        if(!(ToolNow == "upload"))image[14].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
    image[15].addEventListener('mouseover', e => { 
        if(!(ToolNow == "download"))image[15].setAttribute('style','-webkit-filter: brightness(1.2)'); 
    });

    image[15].addEventListener('mouseout', e => { 
        if(!(ToolNow == "download"))image[15].setAttribute('style','-webkit-filter: brightness(1.0)'); 
    });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /*滑鼠點到會變成灰色*/
    function changeToolImage(tool) {
        var i;
        for(i = 0; i < 16; i++) {
            if(i == tool) image[i].setAttribute('style','-webkit-filter: brightness(0.75)'); //如果是就變暗
            else image[i].setAttribute('style','-webkit-filter: brightness(1.0)'); //如果不是就保持原本
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*滑鼠點到會做事情 工具 工具列*/
    
    /*筆*/
    pen.addEventListener('click', e => {
        ToolNow = 'pen';
        change_icon();
        changeToolImage(0);
    });
    /*橡皮擦*/
    eraser.addEventListener('click', e => {
        ToolNow = 'eraser';
        change_icon();
        changeToolImage(1);
    });
    /*螢光筆*/
    highlighter.addEventListener('click', e => {
        ToolNow = 'highlighter';
        change_icon();
        changeToolImage(2);
    });
    /*彩虹筆*/
    rainbow.addEventListener('click', e => {
        ToolNow = 'rainbow';
        // draw_rainbow();
        change_icon();
        changeToolImage(3);
    });
    /*重整*/
    reset.addEventListener('click', e => {
        refresh();
        step = -1;//回到初始(因為push會++)
        while (canvas_history_array.length > 0) canvas_history_array.pop();//把原本的array清空
        push();
        changeToolImage(4);
    });
    /*文字*/
    text.addEventListener('click', e => {
        ToolNow = 'text';
        change_icon();
        changeToolImage(5);
    });
    /*畫直線*/
    line.addEventListener('click', e => {
        ToolNow = 'line';
        change_icon();
        changeToolImage(6);
    });
    /*畫星星*/
    star.addEventListener('click', e => {
        ToolNow = 'star';
        change_icon();
        changeToolImage(7);
    });
    /*畫愛心*/
    heart.addEventListener('click', e => {
        ToolNow = 'heart';
        change_icon();
        changeToolImage(8);
    });
    /*畫圓形*/
    circle.addEventListener('click', e => {
        ToolNow = 'circle';
        change_icon();
        changeToolImage(9);
    });
    /*畫正長方形*/
    rectangle.addEventListener('click', e => {
        ToolNow = 'rectangle';
        change_icon(); 
        changeToolImage(10);
    });
     /*畫三角形*/
    triangle.addEventListener('click', e => {
        ToolNow = 'triangle';
        change_icon();  
        changeToolImage(11);
    });
    /*上一步*/
    undo.addEventListener('click', e => {
        undoo();
        changeToolImage(12);
    });
    /*下一步*/
    redo.addEventListener('click', e => {
        redoo();
        changeToolImage(13);
    });
    /*上傳*/
    //點擊要上傳的檔案成立後(btn有東西了所以被change了)才會跑以下
    //input type很常跟change做連用
    upload.addEventListener('change', e => { 
        ToolNow = 'upload';
        Upload(e);
        changeToolImage(14);
    });
    /*下載*/
    download.addEventListener('click', e => {
        ToolNow = 'download';
        Download();
        changeToolImage(15);
    });
    /*選顏色*/
    color.addEventListener('change', e => {
        choose_color();
    }); 

}
