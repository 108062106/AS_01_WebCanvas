# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | N         |
彩虹筆、造型筆、星星印章、愛心印章、工具列的變化

---

### How to use 
鉛筆：畫畫的工具
橡皮擦：擦除的工具
造型筆：有造型的筆
彩虹筆：有彩虹墨水的筆
直線：畫直線
圓形：畫圓形
正方形：畫正方形
三角形：畫三角形
重整：清空目前的畫布
文字：打文字
上一步：重返上一步
下一步：回去下一步
上傳：可選擇圖片上傳
下載：下載目前的畫布
筆刷：改變字型
字型：改變字的類型
字的大小：改變字的大小
調色盤：改變字的顏色
    

### Function description
1.在畫布上是一般的鼠標，但在右邊選擇工具列卻會是手指樣式的鼠標
2.鼠標移到右側選擇工具列時，被移到的地方會呈現淺灰色
3.選擇此工具時整個工具按鈕會呈現深灰色表示被選取中
4.新增功能:彩虹筆、造型筆
### Gitlab page link

    your web page URL, which should be "https://108062106.gitlab.io/AS_01_WebCanvas"

### Others (Optional)
伊斯蘭恐怖組織ISIS以前叫什麼名稱?
.
.
.
WASWAS
在桃園三結義那天，張飛滿意地看著自己寫的字並轉頭謙虛的對關羽說:
"我字好醜。"
關羽 :
"好醜你好，我字雲長。"
.
.
.
小胖和大胖在草原上騎馬，大胖不知道前面有懸崖
小胖大喊 : "大胖，你快勒馬!"
大胖回頭 : "我很快樂!"
然後大胖就掉下懸崖ㄌ。
.
.
.
謝謝批改 辛苦了 :D

<style>
table th{
    width: 100%;
}
</style>
